from PyQt5.QtWidgets import * 
from PyQt5 import uic
import sys
import json
import os

class font_Controller(QMainWindow):
    def __init__(self) :
        super(font_Controller, self).__init__()
        uic.loadUi("/usr/share/aplikasi/view/font.ui",self)
        self.cmdLinkClock.clicked.connect(self.FontColor_Clock)
        self.cmdLinkDate.clicked.connect(self.FontColor_Date)
        self.cmdLinkBranch.clicked.connect(self.FontColor_Branch)

        self.cmdCancelClock.clicked.connect(self.ClearLink_clock)
        self.cmdCancelDate.clicked.connect(self.ClearLink_date)
        self.cmdCancelBranch.clicked.connect(self.ClearLink_branch)

        self.cmdSave.clicked.connect(self.SavingData)
        self.cmdClose.clicked.connect(self.CloseForm)

        self.selected_font = self.font()
        self.selected_color = self.palette().color(self.backgroundRole())
        
        self.ReadData()

    def FontColor_Clock(self):
        font, ok = QFontDialog.getFont(self.selected_font, self, "Select Font")
        if ok:
            font_info = f"Font: {self.selected_font.family()}, Size: {self.selected_font.pointSize()}"
            self.lineEdit.setText(font_info)

            color = QColorDialog.getColor(self.selected_color, self, "Select Color")
            if color.isValid():
                style_sheet = f"color: {self.selected_color.name()}; font-family: {self.selected_font.family()}; font-size: {self.selected_font.pointSize()}pt;"
                self.lineEdit.setText(style_sheet)

    def FontColor_Date(self):
        font, ok = QFontDialog.getFont(self.selected_font, self, "Select Font")
        if ok:
            font_info = f"Font: {self.selected_font.family()}, Size: {self.selected_font.pointSize()}"
            self.lineEdit_2.setText(font_info)

            color = QColorDialog.getColor(self.selected_color, self, "Select Color")
            if color.isValid():
                style_sheet = f"color: {self.selected_color.name()}; font-family: {self.selected_font.family()}; font-size: {self.selected_font.pointSize()}pt;"
                self.lineEdit_2.setText(style_sheet)

    def FontColor_Branch(self):
        font, ok = QFontDialog.getFont(self.selected_font, self, "Select Font")
        if ok:
            font_info = f"Font: {self.selected_font.family()}, Size: {self.selected_font.pointSize()}"
            self.lineEdit_3.setText(font_info)

            color = QColorDialog.getColor(self.selected_color, self, "Select Color")
            if color.isValid():
                style_sheet = f"color: {self.selected_color.name()}; font-family: {self.selected_font.family()}; font-size: {self.selected_font.pointSize()}pt;"
                self.lineEdit_3.setText(style_sheet)

    def ClearLink_clock(self):
        self.lineEdit.setText("")

    def ClearLink_date(self):
        self.lineEdit_2.setText("")

    def ClearLink_branch(self):
        self.lineEdit_3.setText("")

    def SavingData(self):   
        clock  = self.lineEdit.text()
        date   = self.lineEdit_2.text()
        branch = self.lineEdit_3.text()

        if clock or date or branch :
            data = {'clock':clock, 'date':date, 'branch':branch}

            filename = '/usr/share/aplikasi/data/data_font.json'
            with open(filename, 'w') as file:
                json.dump(data,file)

            QMessageBox.information(None, "Success", "Data Telah Berhasil Disimpan")

    def ReadData(self):
        filename = '/usr/share/aplikasi/data/data_font.json'

        if not os.path.exists(filename):
            with open(filename, 'w') as file:
                file.write("[]")  # Jika file tidak ada, tulis list kosong ke file JSON

        with open(filename, 'r') as file:
            data = json.load(file)
            self.lineEdit.setText(data['clock'])
            self.lineEdit_2.setText(data['date'])
            self.lineEdit_3.setText(data['branch'])

    def CloseForm(self):
        self.close()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = font_Controller()
    window.show()
    sys.exit(app.exec_())

