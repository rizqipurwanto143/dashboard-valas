from PyQt5.QtWidgets import *
from PyQt5 import  uic
import sys
import json

class LanIP_Controller(QMainWindow):
    def __init__(self):
        super(LanIP_Controller,self).__init__()
        uic.loadUi("/usr/share/aplikasi/view/LanIP.ui",self)

    # def SaveData(self):
    #     date    = self.dDate.date()
    #     time    = self.nTime.time()
    #     year    = date.year()
    #     month   = date.month()
    #     day     = date.day()
    #     hour    = time.hour()
    #     minute  = time.minute()
    #     second  = time.second()

    #     data = {'date':f'{year}-{month}-{day}', 'time':f'{hour}:{minute}:{second}'}
    #     filename = 'data/LanIP.json'
    #     with open(filename, 'w') as file:
    #         json.dump(data,file)

    # def CloseForm(self):
    #     self.close()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = LanIP_Controller()
    window.show()
    sys.exit(app.exec_())