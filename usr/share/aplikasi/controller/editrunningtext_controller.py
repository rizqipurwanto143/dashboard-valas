from PyQt5.QtWidgets import *
from PyQt5 import  uic
import sys
import json
import os

class editrunningtext_Controller(QMainWindow):
    def __init__(self,data):
        super(editrunningtext_Controller,self).__init__()
        uic.loadUi("/usr/share/aplikasi/view/editrunningtext.ui",self)
        self.data = data
        self.id_data = ''
        self.cSequence.setText(f"{data}")
        self.cmdLink.clicked.connect(lambda:self.OpenFolder(self.cImage))
        self.cmdFont.clicked.connect(lambda:self.FontColor(self.cFont))

        self.cmdSave.clicked.connect(self.SaveData)
        self.cmdCancel.clicked.connect(self.CloseForm)
        self.selected_font = self.font()
        self.selected_color = self.palette().color(self.backgroundRole())

        self.ReadData()

    def SaveData(self):
        filename = "/usr/share/aplikasi/data/data_runningtext.json"
        data_add = {'sequence':self.cSequence.text(),'image':self.cImage.text(),'text':self.cText.toPlainText(),
                    'font':self.cFont.text(),'active':self.ckActive.isChecked()}
        with open(filename, 'r') as file:
            data = json.load(file)
        
        updated = False
        for item in data:
            if isinstance(item, dict) and 'id' in item and item['id'] == self.id_data:
                item.update(data_add)
                updated = True
                break

        if updated:
            print(f"Item with ID {self.id_data} updated successfully.")
        else:
            print(f"Item with ID {self.id_data} not found.")

        with open(filename, 'w') as file:
            json.dump(data, file, indent=2)

    def OpenFolder(self, name):
        options = QFileDialog.Options()
        file_dialog = QFileDialog()
        file_path, _ = file_dialog.getOpenFileName(self, 'Open File', '', 'All Files (*);;Text Files (*.txt)', options=options)
        
        if file_path:
            name.setText(file_path)
    
    def FontColor(self,name):
        font, ok = QFontDialog.getFont(self.selected_font, self, "Select Font")
        if ok:
            font_info = f"Font: {self.selected_font.family()}, Size: {self.selected_font.pointSize()}"
            name.setText(font_info)

            color = QColorDialog.getColor(self.selected_color, self, "Select Color")
            if color.isValid():
                style_sheet = f"color: {self.selected_color.name()}; font-family: {self.selected_font.family()}; font-size: {self.selected_font.pointSize()}pt;"
                name.setText(style_sheet)

    def ReadData(self):
        data_id = self.data
        filename = "/usr/share/aplikasi/data/data_runningtext.json"
        with open(filename) as file:
            data = json.load(file)
            data = data[data_id]

            self.id_data = data['id']
            self.cSequence.setText(data['sequence'])
            self.cImage.setText(data['image'])
            self.cText.setPlainText(data['text'])
            self.cFont.setText(data['font'])
            self.ckActive.setChecked(data['active'])

    def CloseForm(self):
        self.close()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = editrunningtext_Controller()
    window.show()
    sys.exit(app.exec_())