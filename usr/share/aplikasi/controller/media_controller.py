from PyQt5.QtWidgets import * 
from PyQt5 import uic, QtCore
import sys
import json
import os

class media_Controller(QMainWindow):

    def __init__(self) :
        super(media_Controller, self).__init__()
        uic.loadUi("/usr/share/aplikasi/view/media.ui",self)

        self.saved_text = None

        self.cmdLink.clicked.connect(self.OpenFolder)
        self.cmdSave.clicked.connect(self.SavingData)
        self.cmdCancel.clicked.connect(self.CloseForm)

        self.loaddbGrid()

    def OpenFolder(self):
        options = QFileDialog.Options()
        file_dialog = QFileDialog()
        file_path, _ = file_dialog.getOpenFileName(self, 'Open File', '', 'All Files (*);;Text Files (*.txt)', options=options)
        
        if file_path:
            self.cFillePath.setText(file_path)

    def loaddbGrid(self):
        header  = ['Id','Media']
        data    = [{'Id':'1','Media':'Slide Show'},{'Id':'2','Media':'Video'},{'Id':'3','Media':'Media In'}]

        self.dbGrid.setColumnCount(len(header))
        self.dbGrid.setHorizontalHeaderLabels(header)
        self.dbGrid.verticalHeader().setVisible(False)
        self.dbGrid.setRowCount(len(data))
        self.dbGrid.setColumnWidth(0, 50)  # Lebar Kolom 1
        self.dbGrid.setColumnWidth(1, 245)  # Lebar Kolom 2

        for row, row_data in enumerate(data):
            for column, key in enumerate(header):
                item = QTableWidgetItem(str(row_data[key]))
                item.setFlags(QtCore.Qt.ItemIsEnabled)
                self.dbGrid.setItem(row, column, item)
        self.dbGrid.cellClicked.connect(self.ReadData)
        self.show()

    def SavingData(self):
        id         = self.saved_text
        active     = self.ckActive.isChecked()
        sequence   = self.nSequence.value()
        file_path  = self.cFillePath.text()
        track      = self.nTrack.value()
        duration   = self.nDuration.value()

        filename = "data_media.json"
        data_add = {'id':id, 'active':active, 'sequence':sequence, 'file_path':file_path, 'track': track, 
                    'duration': duration}

        with open(filename, 'r') as file:
            data = json.load(file)

        data.append(data_add)
        with open(filename, 'w') as file:
            json.dump(data, file, indent=4)
        QMessageBox.information(None, "Success", "Data Telah Berhasil Disimpan")
            
    def ReadData(self,row, col):
        filename = '/usr/share/aplikasi/data/data_media.json'
        try:
            with open(filename, 'r') as file:
                self.json_data = json.load(file)
                items = []
                for c in range(self.dbGrid.columnCount()):
                    items.append(self.dbGrid.item(row, c).text())
                    self.saved_text = items[0]
                with open(filename, 'r') as file:
                    data = json.load(file)
                    self.ckActive.setChecked(data[row]['active'])
                    self.nSequence.setValue(data[row]['sequence'])
                    self.cFillePath.setText(data[row]['file_path'])
                    self.nTrack.setValue(data[row]['track'])
                    self.nDuration.setValue(data[row]['duration'])
        except json.JSONDecodeError:
            items = []
            for c in range(self.dbGrid.columnCount()):
                items.append(self.dbGrid.item(row, c).text())

                self.nSequence.setValue(0)
                self.nDuration.setValue(0)
                self.nTrack.setValue(0)
                self.cFillePath.setText("")
                self.saved_text = items[0]
        except Exception as e:
            items = []
            for c in range(self.dbGrid.columnCount()):
                items.append(self.dbGrid.item(row, c).text())

                self.nSequence.setValue(0)
                self.nDuration.setValue(0)
                self.nTrack.setValue(0)
                self.cFillePath.setText("")
                self.saved_text = items[0]

    def CloseForm(self):
        self.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = media_Controller()
    window.show()
    sys.exit(app.exec_())

