from PyQt5.QtWidgets import *
from PyQt5 import  uic
import sys
import json
import os

class addrunningtext_Controller(QMainWindow):
    def __init__(self):
        super(addrunningtext_Controller,self).__init__()
        uic.loadUi("/usr/share/aplikasi/view/addrunningtext.ui",self)
        self.cmdLink.clicked.connect(lambda:self.OpenFolder(self.cImage))
        self.cmdFont.clicked.connect(lambda:self.FontColor(self.cFont))

        self.cmdSave.clicked.connect(self.SaveData)
        self.cmdCancel.clicked.connect(self.CloseForm)
        self.selected_font = self.font()
        self.selected_color = self.palette().color(self.backgroundRole())

    def SaveData(self):
        filename = "data_runningtext.json"
        data_add = {'sequence':self.cSequence.text(),'image':self.cImage.text(),'text':self.cText.toPlainText(),
                    'font':self.cFont.text(),'active':self.ckActive.isChecked()}
        
        if not os.path.exists(filename):
            with open(filename, 'w') as file:
                file.write("[]")  # Jika file tidak ada, tulis list kosong ke file JSON

        with open(filename, 'r') as file:
            data = json.load(file)

        data.append(data_add)
        with open(filename, 'w') as file:
            json.dump(data, file, indent=4)
            QMessageBox.information(None, "Success", "Data Telah Berhasil Disimpan")

    def OpenFolder(self, name):
        options = QFileDialog.Options()
        file_dialog = QFileDialog()
        file_path, _ = file_dialog.getOpenFileName(self, 'Open File', '', 'All Files (*);;Text Files (*.txt)', options=options)
        
        if file_path:
            name.setText(file_path)
    
    def FontColor(self,name):
        font, ok = QFontDialog.getFont(self.selected_font, self, "Select Font")
        if ok:
            font_info = f"Font: {self.selected_font.family()}, Size: {self.selected_font.pointSize()}"
            name.setText(font_info)

            color = QColorDialog.getColor(self.selected_color, self, "Select Color")
            if color.isValid():
                style_sheet = f"color: {self.selected_color.name()}; font-family: {self.selected_font.family()}; font-size: {self.selected_font.pointSize()}pt;"
                name.setText(style_sheet)

    def CloseForm(self):
        self.close()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = addrunningtext_Controller()
    window.show()
    sys.exit(app.exec_())