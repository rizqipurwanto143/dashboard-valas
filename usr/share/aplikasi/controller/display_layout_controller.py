from PyQt5.QtWidgets import *
from PyQt5 import  uic
import sys
import json
import os

class display_layout_Controller(QMainWindow):
    def __init__(self):
        super(display_layout_Controller,self).__init__()
        uic.loadUi("/usr/share/aplikasi/view/display_layout.ui",self)
        self.cmdSave.clicked.connect(self.SaveData)
        self.ReadData()

    def SaveData(self):
        header  = self.nHeader.value()
        media   = self.nMedia.value()
        rate    = self.nRate.value()
        ticker   = self.nTicker.value()

        data = {'header':header, 'media':media, 'rate':rate, 'ticker':ticker}
        filename = '/usr/share/aplikasi/data/data_display_layout.json'
        with open(filename, 'w') as file:
            json.dump(data,file)
            QMessageBox.information(None, "Success", "Data Telah Berhasil Disimpan")

    def ReadData(self):
        filename = '/usr/share/aplikasi/data/data_display_layout.json'

        if not os.path.exists(filename):
            with open(filename, 'w') as file:
                file.write("[]")  # Jika file tidak ada, tulis list kosong ke file JSON

        with open(filename, 'r') as file:
            data = json.load(file)
            self.nHeader.setValue(data['header'])
            self.nMedia.setValue(data['media'])
            self.nRate.setValue(data['rate'])
            self.nTicker.setValue(data['ticker'])

    def CloseForm(self):
        self.close()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = display_layout_Controller()
    window.show()
    sys.exit(app.exec_())