from PyQt5.QtWidgets import *
from PyQt5 import  uic
import sys
import json

class rate_Controller(QMainWindow):
    def __init__(self):
        super(rate_Controller,self).__init__()
        uic.loadUi("/usr/share/aplikasi/view/rate.ui",self)
        self.cmdAdd.clicked.connect(self.AddData)
        self.cmdDelete.clicked.connect(self.DeleteData)
        self.cmdSave.clicked.connect(self.SaveData)
        self.cmdHeaderImage.clicked.connect(lambda:self.ShowFolder("cHeaderImage"))
        self.cmdHeaderFont.clicked.connect(lambda:self.ShowFont("cHeaderFont"))
        self.cmdTItleImage.clicked.connect(lambda:self.ShowFolder("cTextImage"))
        self.cmdTitleFont.clicked.connect(lambda:self.ShowFont("cTextFont"))
        self.cmdCellImage.clicked.connect(lambda:self.ShowFolder("cCellImage"))
        self.cmdCellFont.clicked.connect(lambda:self.ShowFont("cCellFont"))
        self.cmdCancel.clicked.connect(self.CloseForm)

        self.ReadData()


    def AddData(self):
        print("add data")

    def EditData(self):
        print("edit data")

    def DeleteData(self):
        print("add data")

    def ReadData(self):
        print("read data")
    
    def DetailData(self):
        print("Detail data")

    def SaveData(self):
        print("Save Data")

    def ShowFont(self,name):
        print(f"{name}")
    
    def ShowFolder(self,name):
        print(f"{name}")

    def CloseForm(self):
        self.close()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = rate_Controller()
    window.show()
    sys.exit(app.exec_())