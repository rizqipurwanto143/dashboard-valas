from PyQt5.QtWidgets import * 
from PyQt5 import uic
import sys
import json
import os

class imagelink_Controller(QMainWindow):
    def __init__(self) :
        super(imagelink_Controller, self).__init__()
        uic.loadUi("/usr/share/aplikasi/view/imagelink.ui",self)
        
        self.cmdLinkLogo.clicked.connect(self.OpenFolder_Logo)
        self.cmdLinkHeader.clicked.connect(self.OpenFolder_Header)
        self.cmdLinkTicker.clicked.connect(self.OpenFolder_Ticker)
        self.cmdLinkBackground.clicked.connect(self.OpenFolder_Background)

        self.cmdCancelLogo.clicked.connect(self.ClearLink_logo)
        self.cmdCancelHeader.clicked.connect(self.ClearLink_header)
        self.cmdCancelTicker.clicked.connect(self.ClearLink_ticker)
        self.cmdCancelBackground.clicked.connect(self.ClearLink_background)

        self.cmdSave.clicked.connect(self.SavingData)
        self.cmdClose.clicked.connect(self.CloseForm)

        self.selected_font = self.font()
        self.selected_color = self.palette().color(self.backgroundRole())

        self.ReadData()

    def OpenFolder_Logo(self):
        options = QFileDialog.Options()
        file_dialog = QFileDialog()
        file_path, _ = file_dialog.getOpenFileName(self, 'Open File', '', 'All Files (*);;Text Files (*.txt)', options=options)
        
        if file_path:
            self.cLogo.setText(file_path)
            #print(f'Selected file: {file_path}') 

    def OpenFolder_Header(self):
        options = QFileDialog.Options()
        file_dialog = QFileDialog()
        file_path, _ = file_dialog.getOpenFileName(self, 'Open File', '', 'All Files (*);;Text Files (*.txt)', options=options)
        
        if file_path:
            self.cHeader.setText(file_path)

    def OpenFolder_Ticker(self):
        options = QFileDialog.Options()
        file_dialog = QFileDialog()
        file_path, _ = file_dialog.getOpenFileName(self, 'Open File', '', 'All Files (*);;Text Files (*.txt)', options=options)
        
        if file_path:
            self.cTicker.setText(file_path)
    
    def OpenFolder_Background(self):
        options = QFileDialog.Options()
        file_dialog = QFileDialog()
        file_path, _ = file_dialog.getOpenFileName(self, 'Open File', '', 'All Files (*);;Text Files (*.txt)', options=options)
        
        if file_path:
            self.cBackground.setText(file_path)

    def ClearLink_logo(self):
        self.cLogo.setText("")

    def ClearLink_header(self):
        self.cHeader.setText("")

    def ClearLink_ticker(self):
        self.cTicker.setText("")

    def ClearLink_background(self):
        self.cBackground.setText("")

    def SavingData(self):   
        logo        = self.cLogo.text()
        header      = self.cHeader.text()
        ticker      = self.cTicker.text()
        background  = self.cBackground.text()
        header_op   = self.nHeader.value()
        ticker_op   = self.nTicker.value()

        if logo or header or ticker or background:
            data = {'logo':logo, 'header':header, 'ticker':ticker, 'background':background, 'op_header': header_op, 'op_ticker': ticker_op}

            filename = '/usr/share/aplikasi/data/data_imagelink.json'
            with open(filename, 'w') as file:
                json.dump(data,file)

            QMessageBox.information(None, "Success", "Data Telah Berhasil Disimpan")
            
    def ReadData(self):
        filename = '/usr/share/aplikasi/data/data_imagelink.json'

        if not os.path.exists(filename):
            with open(filename, 'w') as file:
                file.write("[]")  # Jika file tidak ada, tulis list kosong ke file JSON

        with open(filename, 'r') as file:
            data = json.load(file)
            self.cLogo.setText(data['logo'])
            self.cHeader.setText(data['header'])
            self.cTicker.setText(data['ticker'])
            self.cBackground.setText(data['background'])
            self.nHeader.setValue(data['op_header'])
            self.nTicker.setValue(data['op_ticker'])

    def CloseForm(self):
        self.close()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = imagelink_Controller()
    window.show()
    sys.exit(app.exec_())

