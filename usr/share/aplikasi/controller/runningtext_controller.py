from PyQt5.QtWidgets import *
from PyQt5 import  uic, QtCore
from addrunningtext_controller import addrunningtext_Controller
from editrunningtext_controller import editrunningtext_Controller
import sys
import json

class runningtext_Controller(QMainWindow):
    def __init__(self):
        super(runningtext_Controller,self).__init__()
        uic.loadUi("/usr/share/aplikasi/view/runningtext.ui",self)
        self.cmdAdd.clicked.connect(self.addData)
        self.cmdEdit.clicked.connect(self.editData)
        self.cmdDelete.clicked.connect(self.deleteData)
        self.LoadDataGrid()
        self.id_data = ''
        self.row_data = ''

    def LoadDataGrid(self):
        with open('/usr/share/aplikasi/data/data_runningtext.json') as data:
            data = json.load(data)
        
        headers = ['Id', 'Seq', 'Text']
        key_data = ['id','sequence','text']
        self.dbGrid.setColumnCount(len(headers))
        self.dbGrid.setHorizontalHeaderLabels(headers)
        self.dbGrid.verticalHeader().setVisible(False)
        self.dbGrid.setRowCount(len(data))
        self.dbGrid.setColumnWidth(0, 50)  # Lebar Kolom 1
        self.dbGrid.setColumnWidth(1, 50)  # Lebar Kolom 2
        self.dbGrid.setColumnWidth(2, 440)  # Lebar Kolom 3

        for row, row_data in enumerate(data):
            for column, key in enumerate(key_data):
                item = QTableWidgetItem(str(row_data[key]))
                item.setFlags(QtCore.Qt.ItemIsEnabled)
                self.dbGrid.setItem(row, column, item)

        self.dbGrid.cellClicked.connect(self.ShowText)
        self.dbGrid.cellDoubleClicked.connect(self.editData)
        self.dbGrid.cellClicked.connect(self.saveIdRow)
        self.show()

    def ShowText(self,row):
        item = self.dbGrid.item(row, 2)
        if item is not None:
            cell_data = item.text()
            self.cData.setText(cell_data)

    def addData(self):
        addForm = addrunningtext_Controller()
        addForm.show()

    def editData(self,row):
        item = self.dbGrid.item(row, 0)
        if item is not None:
            editForm = editrunningtext_Controller(row)
            editForm.show()

    def saveIdRow(self,row):
        item = self.dbGrid.item(row, 0)
        if item is not None:
            self.row_data = row
            self.id_data = item.text()

    def deleteData(self):
        filename = "/usr/share/aplikasi/data/data_runningtext.json"
        with open(filename, 'r') as file:
            data = json.load(file)
            
        deleted = False
        data = [item for item in data if not (isinstance(item, dict) and 'id' in item and item['id'] == self.id_data)]
        print(data)
        if len(data) < len(data):
            deleted = True

        if deleted:
            print(f"Item with ID {self.id_data} deleted successfully.")
        else:
            print(f"Item with ID {self.id_data} not found.")

        # Write the updated data back to the file
        with open(filename, 'w') as file:
            json.dump(data, file, indent=2)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = runningtext_Controller()
    window.show()
    sys.exit(app.exec_())