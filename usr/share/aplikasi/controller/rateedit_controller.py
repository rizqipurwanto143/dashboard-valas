from PyQt5.QtWidgets import *
from PyQt5 import  uic
import sys
import json

class rateedit_Controller(QMainWindow):
    def __init__(self):
        super(rateedit_Controller,self).__init__()
        uic.loadUi("/usr/share/aplikasi/view/rateedit.ui",self)

    # def SaveData(self):
    #     date    = self.dDate.date()
    #     time    = self.nTime.time()
    #     year    = date.year()
    #     month   = date.month()
    #     day     = date.day()
    #     hour    = time.hour()
    #     minute  = time.minute()
    #     second  = time.second()

    #     data = {'date':f'{year}-{month}-{day}', 'time':f'{hour}:{minute}:{second}'}
    #     filename = '/usr/share/aplikasi/data/data_rateedit.json'
    #     with open(filename, 'w') as file:
    #         json.dump(data,file)

    # def CloseForm(self):
    #     self.close()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = rateedit_Controller()
    window.show()
    sys.exit(app.exec_())