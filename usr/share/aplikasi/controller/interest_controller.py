from PyQt5.QtWidgets import *
from PyQt5 import  uic, QtCore
import sys
import json

class interest_Controller(QMainWindow):
    def __init__(self):
        super(interest_Controller,self).__init__()
        uic.loadUi("/usr/share/aplikasi/view/interest.ui",self)
        self.LoadDataGridAtas()

    def LoadDataGridAtas(self):
        with open('data_interest.json') as data:
            data = json.load(data)

        headers = ['Id', 'Description', 'Header']
        self.dbGridAtas.setColumnCount(len(headers))
        self.dbGridAtas.setHorizontalHeaderLabels(headers)
        self.dbGridAtas.verticalHeader().setVisible(False)
        self.dbGridAtas.setRowCount(len(data))
        self.dbGridAtas.setColumnWidth(0, 50)  # Lebar Kolom 1
        self.dbGridAtas.setColumnWidth(1, 245)  # Lebar Kolom 2
        self.dbGridAtas.setColumnWidth(2, 245)  # Lebar Kolom 3

        for row, row_data in enumerate(data):
            for column, key in enumerate(headers):
                item = QTableWidgetItem(str(row_data[key]))
                item.setFlags(QtCore.Qt.ItemIsEnabled)
                self.dbGridAtas.setItem(row, column, item)
        self.dbGridAtas.cellClicked.connect(self.DetailGrid)
        self.show()
    
    def DetailGrid(self, row, col):
        # Mendapatkan data dari sel-sel di baris yang diklik
        with open('data_interest.json') as data:
            data = json.load(data)

        items = []
        for c in range(self.dbGridAtas.columnCount()):
            items.append(self.dbGridAtas.item(row, c).text())

        header_grid2 = json.dumps(data[row]["Title_Text"], indent=2)
        list_output = list(filter(None, header_grid2.split(';')))
        headers = [element.strip('"') for element in list_output]
        
        with open('data_interest_detail.json') as data2:
            data2 = json.load(data2)

        if json.dumps(data2[row]['Data'], indent=2):
            print("ok")
        else:
            print("ko")

    # def SaveData(self):
    #     date    = self.dDate.date()
    #     time    = self.nTime.time()
    #     year    = date.year()
    #     month   = date.month()
    #     day     = date.day()
    #     hour    = time.hour()
    #     minute  = time.minute()
    #     second  = time.second()

    #     data = {'date':f'{year}-{month}-{day}', 'time':f'{hour}:{minute}:{second}'}
    #     filename = 'data/interest.json'
    #     with open(filename, 'w') as file:
    #         json.dump(data,file)

    # def CloseForm(self):
    #     self.close()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = interest_Controller()
    window.show()
    sys.exit(app.exec_())