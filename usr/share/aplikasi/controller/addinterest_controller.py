from PyQt5.QtWidgets import *
from PyQt5 import  uic
import sys
import json
import os

class addinterest_Controller(QMainWindow):
    def __init__(self):
        super(addinterest_Controller,self).__init__()
        uic.loadUi("/usr/share/aplikasi/view/addinterest.ui",self)

        #Action Form
        self.cmdSave.clicked.connect(self.SaveData)
        self.cmdCancel.clicked.connect(self.CloseForm)
        
        #Load Font
        self.cmdFontHeader.clicked.connect(lambda:self.LoadFontColor(self.cHeaderFont))
        self.cmdFontTitle.clicked.connect(lambda:self.LoadFontColor(self.cTitleFont))
        self.cmdFontCell.clicked.connect(lambda:self.LoadFontColor(self.cCellFont))

        #Load File

        self.cmdLinkHeader.clicked.connect(lambda:self.GetNameFile(self.cHeaderImage))
        self.cmdLinkTitle.clicked.connect(lambda:self.GetNameFile(self.cTitleImage))
        self.cmdLinkCell.clicked.connect(lambda:self.GetNameFile(self.cCellImage))

        #Clear Field

        self.cmdClearHeader.clicked.connect(lambda:self.ClearField(self.cHeaderImage))
        self.cmdClearTitle.clicked.connect(lambda:self.ClearField(self.cTitleImage))
        self.cmdClearCell.clicked.connect(lambda:self.ClearField(self.cCellImage))

        self.selected_font = self.font()
        self.selected_color = self.palette().color(self.backgroundRole())

    def SaveData(self):
        Id              = self.cId.text()
        Duration        = self.cDuration.text()
        Name            = self.cName.text()
        Panel           = self.slPanel.currentText()
        Active          = self.ckActive.isChecked()
        Sequence        = self.cSequence.text()
        Header_Text     = self.cHeaderText.toPlainText()
        Header_Image    = self.cHeaderImage.text()
        Header_Font     = self.cHeaderFont.text()
        Header_Height   = self.nHeaderHeight.value()
        Header_Opacity  = self.nHeaderOpacity.value()
        Title_Text      = self.nTitleText.toPlainText()
        Title_Image     = self.cTitleImage.text()
        Title_Font      = self.cTitleFont.text()
        Title_Height    = self.nTitleHeight.value()
        Title_Opacity   = self.nTitleOpacity.value()
        Cell_Image      = self.cCellImage.text()
        Cell_Font       = self.cCellFont.text()
        Cell_Opacity    = self.nCellOpacity.value()
         
        data_add = {'Id':Id,'Duration':Duration,'Description':Name,'Panel':Panel,'Active':Active,'Sequence':Sequence,
                    'Header':Header_Text,'Header_Image':Header_Image,'Header_Font':Header_Font,'Header_Height':Header_Height,
                    'Header_Opacity':Header_Opacity,'Title_Text':Title_Text,'Title_Image':Title_Image,'Title_Font':Title_Font,
                    'Title_Height':Title_Height,'Title_Opacity':Title_Opacity,'Cell_Image':Cell_Image,'Cell_Fotn':Cell_Font,
                    'Cell_Opacity':Cell_Opacity}
        filename = '/usr/share/aplikasi/data/data_interest.json'

        if not os.path.exists(filename):
            
            with open(filename, 'w') as file:
                file.write("[]")  # Jika file tidak ada, tulis list kosong ke file JSON

        with open(filename, 'r') as file:
            data = json.load(file)

        data.append(data_add)
        with open(filename, 'w') as file:
            json.dump(data, file, indent=4)

    def LoadFontColor(self,cName):
        font, ok = QFontDialog.getFont(self.selected_font, self, "Select Font")
        if ok:
            font_info = f"Font: {self.selected_font.family()}, Size: {self.selected_font.pointSize()}"
            cName.setText(font_info)

            color = QColorDialog.getColor(self.selected_color, self, "Select Color")
            if color.isValid():
                style_sheet = f"color: {self.selected_color.name()}; font-family: {self.selected_font.family()}; font-size: {self.selected_font.pointSize()}pt;"
                cName.setText(style_sheet)

    def GetNameFile(self,cName):
        options = QFileDialog.Options()
        file_dialog = QFileDialog()
        file_path, _ = file_dialog.getOpenFileName(self, 'Open File', '', 'All Files (*);;Text Files (*.txt)', options=options)
        
        if file_path:
            cName.setText(file_path)

    def ClearField(self,cName):
        cName.setText("")

    def CloseForm(self):
        self.close()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = addinterest_Controller()
    window.show()
    sys.exit(app.exec_())