import sys
import os
sys.path.append('/home/x-core/dashboard-valas/usr/share/')
from aplikasi.controller.rate_controller import rate_Controller as mnuRate

from PyQt5.QtWidgets import *
from PyQt5 import uic
import json

class main_Controller(QMainWindow):
    def __init__(self):
        super(main_Controller,self).__init__()
        uic.loadUi("/usr/share/aplikasi/view/dashboard.ui",self)
        # self.img_link.clicked.connect(self.openimagelink)
        self.rate.clicked.connect(self.openRate)

    def openRate(self):
        addForm = mnuRate()
        addForm.show()

if __name__ == '__main__':
    app = QApplication([sys.argv, '--no-sandbox'])
    window = main_Controller()
    window.show()
    sys.exit(app.exec_())